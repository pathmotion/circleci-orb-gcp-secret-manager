# circleci-orb-gcp-secret-manager

[![CircleCI](https://circleci.com/bb/pathmotion/circleci-orb-gcp-secret-manager.svg?style=svg&circle-token=a09989f1c23c345e572249fd005ee7b91d0f0d28)](https://app.circleci.com/pipelines/bitbucket/pathmotion/circleci-orb-gcp-secret-manager)
[![CircleCI Orb Version](https://img.shields.io/badge/endpoint.svg?url=https://badges.circleci.io/orb/pathmotion/gcp-secret-manager)](https://circleci.com/orbs/registry/orb/pathmotion/gcp-secret-manager)

A CircleCI Orb to access a secret from Google Cloud Platform Secret Manager.

## Features

This orb offers the ability to retrieve a secret from GCP Secret Manager. The secret is exported to a env file.

It also offers the ability to source any file with environment variables in it.

### Executors

#### default

Google SDK Docker image [google/cloud-sdk](https://hub.docker.com/r/google/cloud-sdk/).

##### Parameters

| Parameter | type | default |
|-----------|------|---------|
| `tag` | `string` | `alpine` |

### Commands

You may use the following commands provided by this orb directly from your own job.

- [**access-secret-version**](#access-secret-version) - Retrieve a secret from GCP Secret Manager

- [**source-env-file**](#source-env-file) - Source an env file

#### `access-secret-version`

##### Parameters

| Parameter | type | default | description |
|-----------|------|---------|-------------|
| `secret-version` | `string` | `latest` | Version of the secret |
| `secret-name` | `string` | - | Name of the secret |
| `create-secrets-directory` | `boolean` | `true` | Create a directory where the env file will be created |
| `secrets-directory` | `string` | `.secrets`| Name of the directory to create the env file |
| `secret-file` | `string` | `secrets.env` | The name of the file where to extracts the secret into a variable |

##### Example

```yaml
version: 2.1
orbs:
  gcp-sm: pathmotion/gcp-secret-manager@0.0.1
  gcp-cli: circleci/gcp-cli@1.8.4
jobs:
  access-gcp-secret-version:
    executor: gcp-sm/default
    steps:
      - gcp-cli/initialize
      - gcp-sm/access-secret-version:
          secret-name: MY_API_TOKEN
      - persist_to_workspace:
          root: .
          paths:
            - .secrets/secrets.env
    environment:
      GOOGLE_PROJECT_ID: my-gcp-project-id
      GOOGLE_COMPUTE_ZONE: my-gcp-zone
workflows:
  version: 2
  example-workflow:
    jobs:
      - access-gcp-secret-version
```

#### `access-secret-version`

##### Parameters

| Parameter | type | default | description |
|-----------|------|---------|-------------|
| `secrets-directory` | `string` | `.secrets`| Name of the directory to create the env file |
| `secret-file` | `string` | `secrets.env` | The name of the file where to extracts the secret into a variable |

##### Example

```yaml
version: 2.1
orbs:
  gcp-sm: pathmotion/gcp-secret-manager@0.0.1
workflows:
  version: 2
  example-workflow:
    jobs:
      - gcp-sm/source-env-file
```

## Annexes

[Google Cloud Platform Secret Manager Documentation](https://cloud.google.com/secret-manager/docs)
